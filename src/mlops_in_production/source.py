from typing import Iterable, TypeVar

T = TypeVar("T")


def summator(iterable: Iterable[T]) -> T:
    """Calculate sum of passed iterable

    Args:
        iterable (Iterable[T]): Iterable to calculate sum

    Returns:
        Sum of iterable elements
    """
    return sum(iterable)


if __name__ == "__main__":
    print(summator((1, 2, 3)))
