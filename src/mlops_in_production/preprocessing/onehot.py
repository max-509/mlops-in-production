from __future__ import annotations

from typing import Sequence

import numpy as np
import pandas as pd
from sklearn.preprocessing import (
    LabelEncoder,
    OneHotEncoder,
    OrdinalEncoder,
    StandardScaler,
)

from mlops_in_production.dataset.metadata import (
    TableDataset,
    TableMetadata,
    TableTaskType,
)
from mlops_in_production.preprocessing.preprocessing import TablePreprocessor


class OheTablePreprocessor(TablePreprocessor):
    num_scaler_: StandardScaler
    cat_encoder_: OneHotEncoder
    bin_encoder_: OrdinalEncoder
    target_preprocessor_: LabelEncoder | StandardScaler
    original_bin_features_: Sequence[str]
    ohe_features_: Sequence[str]

    def fit(self, dataset: TableDataset) -> OheTablePreprocessor:
        df = dataset.train_df
        self.input_metadata_ = dataset.metadata

        num_features = self.input_metadata_.num_features
        cat_features = self.input_metadata_.cat_features
        bin_features = self.input_metadata_.bin_features
        target_col = self.input_metadata_.target_col
        task_type = self.input_metadata_.task_type

        print(type(num_features))
        print(df)
        self.num_scaler_ = StandardScaler().fit(df[num_features])

        self.cat_encoder_ = OneHotEncoder(
            handle_unknown="ignore",
            sparse_output=False,
            dtype=np.int32,
        ).fit(df[cat_features])

        self.bin_encoder_ = OrdinalEncoder(
            handle_unknown="use_encoded_value",
            unknown_value=-1,
            encoded_missing_value=-1,
            dtype=np.int32,
        ).fit(df[bin_features])

        self.target_preprocessor_ = (
            StandardScaler()
            if task_type is TableTaskType.REGRESSION
            else LabelEncoder()
        ).fit(df[target_col].to_numpy().reshape(-1, 1))

        self.ohe_features_ = [
            f"{cat_feature}_{cat_value}"
            for cat_idx, cat_feature in enumerate(cat_features)
            for cat_value in self.cat_encoder_.categories_[cat_idx]
        ]
        self.original_bin_features_ = bin_features

        new_bin_features = bin_features + self.ohe_features_
        new_cat_features = []
        self.output_metadata_ = TableMetadata(
            num_features=num_features,
            bin_features=new_bin_features,
            cat_features=new_cat_features,
            target_col=target_col,
            task_type=task_type,
        )

        return self

    def transform(self, df: pd.DataFrame) -> pd.DataFrame:
        df = df.copy()
        df[self.output_metadata_.num_features] = self.num_scaler_.transform(
            df[self.input_metadata_.num_features]
        )

        df[self.ohe_features_] = self.cat_encoder_.transform(
            df[self.input_metadata_.cat_features]
        )
        df = df.drop(self.input_metadata_.cat_features, axis=1)

        df[self.original_bin_features_] = self.bin_encoder_.transform(
            df[self.input_metadata_.bin_features]
        )

        if self.input_metadata_.target_col in df.columns:
            df[self.output_metadata_.target_col] = self.target_preprocessor_.transform(
                df[self.input_metadata_.target_col].to_numpy().reshape(-1, 1)
            ).ravel()

        return df
