from __future__ import annotations

from abc import ABC, abstractmethod
from typing import Sequence

import numpy as np
import pandas as pd
from sklearn.preprocessing import (
    LabelEncoder,
    OneHotEncoder,
    OrdinalEncoder,
    StandardScaler,
)

from mlops_in_production.dataset.metadata import (
    TableDataset,
    TableMetadata,
    TableTaskType,
)


class TablePreprocessor(ABC):
    input_metadata_: TableMetadata
    output_metadata_: TableMetadata

    @abstractmethod
    def fit(self, dataset: TableDataset) -> TablePreprocessor: ...

    @abstractmethod
    def transform(self, df: pd.DataFrame) -> pd.DataFrame: ...
