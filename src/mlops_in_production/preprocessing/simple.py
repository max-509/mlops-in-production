from __future__ import annotations

from abc import ABC, abstractmethod
from typing import Sequence

import numpy as np
import pandas as pd
from sklearn.preprocessing import (
    LabelEncoder,
    OneHotEncoder,
    OrdinalEncoder,
    StandardScaler,
)

from mlops_in_production.dataset.metadata import (
    TableDataset,
    TableMetadata,
    TableTaskType,
)
from mlops_in_production.preprocessing.preprocessing import TablePreprocessor


class SimpleTablePreprocessor(TablePreprocessor):
    num_scaler_: StandardScaler
    cat_encoder_: OrdinalEncoder
    target_preprocessor_: LabelEncoder | StandardScaler

    def fit(self, dataset: TableDataset) -> SimpleTablePreprocessor:
        df = dataset.train_df
        self.input_metadata_ = dataset.metadata

        num_features = self.input_metadata_.num_features
        cat_features = self.input_metadata_.cat_features
        bin_features = self.input_metadata_.bin_features
        target_col = self.input_metadata_.target_col
        task_type = self.input_metadata_.task_type

        self.num_scaler_ = StandardScaler().fit(df[num_features])
        self.cat_encoder_ = OrdinalEncoder(
            handle_unknown="use_encoded_value",
            unknown_value=-1,
            encoded_missing_value=-1,
            dtype=np.int32,
        ).fit(df[cat_features + bin_features])
        self.target_preprocessor_ = (
            StandardScaler()
            if task_type is TableTaskType.REGRESSION
            else LabelEncoder()
        ).fit(df[target_col].to_numpy().reshape(-1, 1))

        self.output_metadata_ = self.input_metadata_

        return self

    def transform(self, df: pd.DataFrame) -> pd.DataFrame:
        df = df.copy()
        df[self.output_metadata_.num_features] = self.num_scaler_.transform(
            df[self.input_metadata_.num_features]
        )

        df[self.output_metadata_.cat_features + self.output_metadata_.bin_features] = (
            self.cat_encoder_.transform(
                df[
                    self.input_metadata_.cat_features
                    + self.input_metadata_.bin_features
                ]
            )
        )

        if self.input_metadata_.target_col in df.columns:
            df[self.output_metadata_.target_col] = self.target_preprocessor_.transform(
                df[self.input_metadata_.target_col].to_numpy().reshape(-1, 1)
            ).ravel()

        return df
