import json
import pathlib
from dataclasses import asdict

import pandas as pd

from mlops_in_production.dataset.metadata import TableDataset


def save_table_dataset(
    dataset: TableDataset, table_format: str, output_path: pathlib.Path
) -> None:
    output_path.mkdir(exist_ok=True)
    print(dataset)

    save_func = {
        "csv": pd.DataFrame.to_csv,
        "parquet": pd.DataFrame.to_parquet,
        "orc": pd.DataFrame.to_orc,
    }
    save_func[table_format](dataset.train_df, output_path / f"train.{table_format}")
    if dataset.val_df is not None:
        save_func[table_format](dataset.val_df, output_path / f"val.{table_format}")

    with open(str(output_path / "metadata.json"), "w") as metadata_f:
        json.dump(asdict(dataset.metadata), metadata_f)
