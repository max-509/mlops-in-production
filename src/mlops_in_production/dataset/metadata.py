from dataclasses import dataclass
from enum import Enum
from typing import Sequence

import pandas as pd


class TableTaskType(str, Enum):
    CLASSIFICATION = "CLASSIFICATION"
    REGRESSION = "REGRESSION"


@dataclass(frozen=True)
class TableMetadata:
    num_features: Sequence[str]
    cat_features: Sequence[str]
    bin_features: Sequence[str]
    target_col: str
    task_type: TableTaskType


@dataclass(frozen=True)
class TableDataset:
    train_df: pd.DataFrame
    metadata: TableMetadata
    val_df: pd.DataFrame | None = None
