import json
import pathlib

import pandas as pd
from dacite import Config, from_dict

from mlops_in_production.dataset.metadata import (
    TableDataset,
    TableMetadata,
    TableTaskType,
)


def read_table_dataset(
    dataset_path: pathlib.Path, table_format: str, **reader_params
) -> TableDataset:
    read_func = {
        "csv": pd.read_csv,
        "parquet": pd.read_parquet,
        "orc": pd.read_orc,
    }
    train_df = read_func[table_format](
        str(dataset_path / f"train.{table_format}"), **reader_params
    )
    val_df = None
    if f"val.{table_format}" in set(p.name for p in dataset_path.iterdir()):
        val_df = read_func[table_format](
            str(dataset_path / f"val.{table_format}"), **reader_params
        )

    with open(str(dataset_path / "metadata.json"), "r") as metadata_f:
        metadata = from_dict(
            data_class=TableMetadata,
            data=json.load(metadata_f),
            config=Config(cast=[TableTaskType]),
        )

    return TableDataset(train_df=train_df, metadata=metadata, val_df=val_df)
