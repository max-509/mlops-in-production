from __future__ import annotations

from typing import Any, Literal

import pandas as pd
from catboost import CatBoost, CatBoostClassifier, CatBoostRegressor, Pool

from mlops_in_production.dataset.metadata import TableDataset, TableTaskType
from mlops_in_production.modeling.modeling import TableModel


class CatboostTableModel(TableModel):
    features_: list[str]
    cat_features_: list[str]
    model_: CatBoost

    def __init__(
        self,
        seed: int | None = None,
        iterations: int | None = None,
        learning_rate: float | None = None,
        depth: int | None = None,
        l2_leaf_reg: float | None = None,
        bagging_temperature: float | None = None,
        auto_class_weights: Literal["Balanced", "SqrtBalanced"] | None = None,
        use_best_model: bool | None = None,
    ):
        super().__init__(seed)

        self._iterations = iterations
        self._learning_rate = learning_rate
        self._depth = depth
        self._l2_leaf_reg = l2_leaf_reg
        self._bagging_temperature = bagging_temperature
        self._auto_class_weights = auto_class_weights
        self._use_best_model = use_best_model

    def fit(
        self, train_dataset: TableDataset, parallelism: int = 1
    ) -> CatboostTableModel:
        dataset_metadata = train_dataset.metadata

        catboost_hparams: dict[str, Any] = {
            "iterations": self._iterations,
            "learning_rate": self._learning_rate,
            "depth": self._depth,
            "l2_leaf_reg": self._l2_leaf_reg,
            "bagging_temperature": self._bagging_temperature,
            "auto_class_weights": self._auto_class_weights,
            "use_best_model": self._use_best_model,
        }

        self.cat_features_ = dataset_metadata.cat_features

        self.model_ = (
            CatBoostClassifier
            if dataset_metadata.task_type is TableTaskType.CLASSIFICATION
            else CatBoostRegressor
        )(
            cat_features=dataset_metadata.cat_features,
            random_seed=self._seed,
            thread_count=parallelism,
            **catboost_hparams,
        )

        self.features_ = (
            dataset_metadata.num_features
            + dataset_metadata.cat_features
            + dataset_metadata.bin_features
        )
        train_pool = Pool(
            data=train_dataset.train_df[self.features_],
            label=train_dataset.train_df[dataset_metadata.target_col],
            cat_features=dataset_metadata.cat_features,
            feature_names=self.features_,
        )

        val_pool = None
        if train_dataset.val_df is not None:
            val_pool = Pool(
                data=train_dataset.val_df[self.features_],
                label=train_dataset.val_df[dataset_metadata.target_col],
                cat_features=dataset_metadata.cat_features,
                feature_names=self.features_,
            )

        self.model_.fit(X=train_pool, eval_set=val_pool, verbose=True)

        return self

    def predict(self, df: pd.DataFrame) -> pd.Series:
        df_pool = Pool(
            data=df[self.features_],
            cat_features=self.cat_features_,
            feature_names=self.features_,
        )
        predictions = pd.Series(self.model_.predict(df_pool, verbose=True))

        return predictions
