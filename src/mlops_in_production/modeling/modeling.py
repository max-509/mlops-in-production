from __future__ import annotations

from abc import ABC

import pandas as pd

from mlops_in_production.dataset.metadata import TableDataset


class TableModel(ABC):
    def __init__(self, seed: int | None = None):
        self._seed = seed

    def fit(self, train_dataset: TableDataset, parallelism: int = 1) -> TableModel: ...

    def predict(self, df: pd.DataFrame) -> pd.Series: ...
