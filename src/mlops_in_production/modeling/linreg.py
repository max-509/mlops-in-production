from __future__ import annotations

import os
from functools import partial
from typing import Any, Literal

import numpy as np
import pandas as pd
from sklearn.linear_model import SGDClassifier, SGDRegressor

from mlops_in_production.dataset.metadata import TableDataset, TableTaskType
from mlops_in_production.modeling.modeling import TableModel


class LinregTableModel(TableModel):
    features_: list[str]
    model_: SGDRegressor | SGDClassifier

    def __init__(
        self,
        seed: int | None = None,
        loss: str | None = None,
        penalty: str = Literal["l2", "l1", "elasticnet"],
        alpha: float = 0.0001,
        l1_ratio: float = 0.15,
        fit_intercept: bool = True,
        max_iter: int | None = None,
        tol: float = 0.001,
        shuffle: bool = True,
        verbose: int = 0,
        learning_rate: Literal[
            "constant", "optimal", "invscaling", "adaptive"
        ] = "optimal",
        eta0: float = 0.1,
    ):
        super().__init__(seed)

        self._loss = loss
        self._penalty = penalty
        self._alpha = alpha
        self._l1_ratio = l1_ratio
        self._fit_intercept = fit_intercept
        self._max_iter = max_iter
        self._tol = tol
        self._shuffle = shuffle
        self._verbose = verbose
        self._learning_rate = learning_rate
        self._eta0 = eta0

    def fit(
        self, train_dataset: TableDataset, parallelism: int = 1
    ) -> LinregTableModel:
        dataset_metadata = train_dataset.metadata

        linreg_hparams: dict[str, Any] = {
            "penalty": self._penalty,
            "alpha": self._alpha,
            "l1_ratio": self._l1_ratio,
            "fit_intercept": self._fit_intercept,
            "tol": self._tol,
            "shuffle": self._shuffle,
            "verbose": self._verbose,
            "learning_rate": self._learning_rate,
            "eta0": self._eta0,
        }
        if self._loss is not None:
            linreg_hparams["loss"] = self._loss

        full_df = train_dataset.train_df
        if train_dataset.val_df is not None:
            train_size = len(train_dataset.train_df)
            val_size = len(train_dataset.val_df)
            val_fraction = val_size / (train_size + val_size)

            linreg_hparams["early_stopping"] = True
            linreg_hparams["validation_fraction"] = val_fraction

            full_df = pd.concat((train_dataset.train_df, train_dataset.val_df), axis=0)

        max_iter = self._max_iter
        if max_iter is None:
            max_iter = np.ceil(10**6 / len(full_df))
        self.model_ = (
            partial(SGDClassifier, n_jobs=parallelism)
            if dataset_metadata.task_type is TableTaskType.CLASSIFICATION
            else SGDRegressor
        )(
            random_state=self._seed,
            **linreg_hparams,
        )
        if dataset_metadata.task_type is TableTaskType.REGRESSION:
            # Linear regression with regularization parallelizes only by this env variable
            os.environ["OMP_NUM_THREADS"] = parallelism

        self.features_ = (
            dataset_metadata.num_features
            + dataset_metadata.cat_features
            + dataset_metadata.bin_features
        )
        self.model_.fit(
            X=full_df[self.features_], y=full_df[dataset_metadata.target_col]
        )

        return self

    def predict(self, df: pd.DataFrame) -> pd.Series:
        predictions = pd.Series(self.model_.predict(df[self.features_]))

        return predictions
