import pandas as pd
from sklearn.model_selection import train_test_split

from mlops_in_production.dataset.metadata import TableDataset, TableTaskType


def train_val_split(
    full_dataset: TableDataset,
    stratified: bool,
    val_size: float = 0.8,
    seed: int | None = None,
) -> TableDataset:
    if full_dataset.val_df is not None:
        raise ValueError("Validation dataframe must be None")
    full_df = full_dataset.train_df
    target_col = full_dataset.metadata.target_col
    task_type = full_dataset.metadata.task_type
    X = full_df.drop(target_col, axis=1)
    features = X.columns
    y = full_df[target_col]

    stratify = None
    if stratified and task_type is TableTaskType.CLASSIFICATION:
        stratify = y

    X_train, X_val, y_train, y_val = train_test_split(
        X, y, test_size=val_size, random_state=seed, stratify=stratify
    )

    train_df = pd.DataFrame(X_train, columns=features)
    train_df[target_col] = y_train.to_numpy().reshape(-1)

    val_df = pd.DataFrame(X_val, columns=features)
    val_df[target_col] = y_val.to_numpy().reshape(-1)

    return TableDataset(
        train_df=train_df, metadata=full_dataset.metadata, val_df=val_df
    )
