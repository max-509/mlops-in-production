import pathlib


def get_project_path() -> pathlib.Path:
    return pathlib.Path(__file__).parent.parent


def get_src_path() -> pathlib.Path:
    return get_project_path().parent


def get_repo_path() -> pathlib.Path:
    return get_src_path().parent
