FROM mambaorg/micromamba as ci_prod_build

#python
ENV PYTHONFAULTHANDLER=1 \
  PYTHONUNBUFFERED=1 \
  PYTHONHASHSEED=random \
  # pip:
  PIP_NO_CACHE_DIR=off \
  PIP_DISABLE_PIP_VERSION_CHECK=on \
  PIP_DEFAULT_TIMEOUT=100 \
  # Poetry's configuration:
  POETRY_NO_INTERACTION=1 \
  POETRY_VIRTUALENVS_CREATE=false

USER root
RUN apt-get update && apt-get upgrade -y \
  && apt-get install --no-install-recommends -y \
  curl unzip \
  # Cleaning cache:
  && apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false \
  && apt-get clean -y && rm -rf /var/lib/apt/lists/*

USER $MAMBA_USER
COPY env.yml /tmp
RUN micromamba install -f /tmp/env.yml \
  && micromamba run poetry --version

WORKDIR /app

COPY poetry.lock pyproject.toml /app/
RUN micromamba run poetry install --only main --no-root

FROM ci_prod_build as ci_dev_build
RUN micromamba run poetry install --no-root

FROM ci_dev_build as ci_docs_build
COPY README.md /app
COPY CONTRIBUTING.md /app
COPY mkdocs.yml /app
COPY src /app/src
COPY docs /app/docs
COPY experiments /docs/experiments
RUN micromamba run poetry install --with docs --no-root

FROM ci_prod_build as prod_build
COPY src /app/src
COPY README.md /app
RUN micromamba run poetry install --compile --only main

FROM ci_dev_build as dev_build
ENV KAGGLE_key=53aacb24655938caeba169e30e2b2c9b \
  KAGGLE_username=maximvershinin
COPY tests /app/tests
RUN micromamba run poetry install --with dev
