#!/usr/bin/env bash

poetry run -n kaggle datasets download new-york-city/ny-2015-street-tree-census-tree-data
unzip ny-2015-street-tree-census-tree-data.zip -d data
mv data/2015-street-tree-census-tree-data.csv ./
rm -rf data ny-2015-street-tree-census-tree-data.zip
