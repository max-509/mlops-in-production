### How to check your changes

You can use `pre-commit` tool for code style and errors check.
1. Add code changes to git index: `git add path/to/changes`
2. Run command `pre-commit run -a`

For automatic checks your code before commits and pushes, you need install pre-commit as git hooks: `pre-commit install`.
After that each your commit and push attempt will run pre-commit checks.

### VSCode configuration

For VSCode configuration you need install [python](https://marketplace.visualstudio.com/items?itemName=ms-python.python) and [ruff](https://marketplace.visualstudio.com/items?itemName=charliermarsh.ruff) plugins.
