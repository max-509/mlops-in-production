# MLOPS in production course

## How to install

For project installation you need *poetry* compatible with 1.8.0 version. You can find official installation guide [here](https://python-poetry.org/docs/#installing-with-the-official-installer). After just run `poetry install`

## Project methodology

For contributing you need to create pull request to **main** branch. You can create next types of pull requests:
1. Experiment pull request;
2. Data pull request;
3. Feature pull request;
4. Configuration pull request;
5. Bug pull request;

### Experiment
Each experiment should be carried out in single `.ipynb` file in [experiments](./experiments/) directory. Pull request should have *experiment/* prefix. After approving experiment correctness of this experiment, it will be merged to main. Experiment shouldn't make any changes in project source code and in configuration.

Successful experiment can be transformed to *data* and *feature* pull request. Contributor should use *data* pull request if your experiment make some data transformations, and *feature* pull request if your experiment make modeling improvements.

### Data
This pull request need for adding data dependency to this project. Pull request should have *data/* prefix.

### Feature
This pull request need for adding some new or improving current functionality for this project. Pull request should have *feature/* prefix.

### Configuration
This pull request need for changing existing configuration for this project. Pull request should have *configuration/* prefix.

### Bug
This pull request need for fixing project bugs. Pull request should have *bug/* prefix.

## Docker build

You can build docker image with current project dependencies. You can build **prod** and **dev** image versions. **dev** version is same with **prod** version, but additionally contains test dependencies and test code.

For **dev** image, you need to run next command:
```shell
docker build --target dev_build -t <your tag>
```
For **prod** image, you need to run next command:
```shell
docker build --target prod_build -t <your tag>
```

## Snakemake

You can run workflow of **adult** dataset training with different preprocessors and models. For this you need to change [table](config/adult_modeling.tsv) with
preprocessing and modeling parameters and set some hyperparameters [config file](config/config.yaml).

For running command enter in your shell:
```snakemake```
Preprocessors and models will be in [results directory](workflow/results/), datasets will be in [data directory](data/).

![Constructed DAG](image/adult_dag.svg)
