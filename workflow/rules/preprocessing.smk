rule preprocess_table_dataset:
    input:
        "data/{dataset}_{table_format}_format{postfix}",
    params:
        table_format=lambda w: w.table_format,
        preprocessor_name=lambda w: w.preprocessor_type,
        seed=config["table_params"]["seed"],
    output:
        dataset=directory(
            "data/{dataset}_{table_format}_format{postfix}_{preprocessor_type}_pp"
        ),
        preprocessor="workflow/results/{dataset}_{table_format}_format{postfix}_{preprocessor_type}.preprocessor.pickle",
    log:
        out="workflow/logs/preprocess_table_dataset_{dataset}_{table_format}_format{postfix}_{preprocessor_type}.log",
        err="workflow/logs/preprocess_table_dataset_{dataset}_{table_format}_format{postfix}_{preprocessor_type}.err",
    script:
        "../scripts/preprocess_table.py"
