rule adult_dataset_downloading:
    output:
        temp("workflow/results/adult-income-dataset.zip"),
    params:
        user_name="wenruliu",
        dataset_name="adult-income-dataset",
    log:
        out="workflow/logs/adult_dataset_downloading_stdout.log",
        err="workflow/logs/adult_dataset_downloading_stderr.err",
    script:
        "../scripts/kaggle_download_dataset.py"


rule adult_dataset_reading:
    input:
        "workflow/results/adult-income-dataset.zip",
    output:
        directory("data/adult_{table_format}_format"),
    params:
        table_format=lambda w: w.table_format,
    log:
        out="workflow/logs/adult_dataset_reading_{table_format}_stdout.log",
        err="workflow/logs/adult_dataset_reading_{table_format}_stderr.err",
    script:
        "../scripts/read_adult_dataset.py"


rule split_table_dataset:
    input:
        "data/{dataset_name}_{table_format}_format",
    params:
        table_format=lambda w: w.table_format,
        stratified=config["table_params"]["stratified"],
        val_size=config["table_params"]["val_size"],
        seed=config["table_params"]["seed"],
    output:
        directory("data/{dataset_name}_{table_format}_format_train_val_split"),
    log:
        out="workflow/logs/split_table_dataset_{dataset_name}_{table_format}_stdout.log",
        err="workflow/logs/split_table_dataset_{dataset_name}_{table_format}_stderr.err",
    script:
        "../scripts/train_val_split_table.py"
