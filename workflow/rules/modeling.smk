import os


rule train_table_dataset:
    input:
        "data/{dataset}_{table_format}_format{postfix}",
    params:
        table_format=lambda w: w.table_format,
        model_name=lambda w: w.model_type,
        seed=config["table_params"]["seed"],
    output:
        model="workflow/results/{dataset}_{table_format}_format{postfix}_{model_type}.model.pickle",
    log:
        out="workflow/logs/train_table_dataset_{dataset}_{table_format}_format{postfix}_{model_type}.log",
        err="workflow/logs/train_table_dataset_{dataset}_{table_format}_format{postfix}_{model_type}.err",
    threads: int(os.environ["MAX_CORES"]) // 2
    script:
        "../scripts/train_table.py"
