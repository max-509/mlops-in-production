import pandas as pd
from snakemake.utils import min_version
from snakemake.utils import validate

min_version("7.0")


configfile: "config/snakemake/config.yaml"


samples = pd.read_table(config["adult_modeling"]).set_index("name", drop=False)
validate(samples, "schema/adult_modeling.schema.yaml")


##### load rules #####
include: "rules/data_reading.smk"
include: "rules/preprocessing.smk"
include: "rules/modeling.smk"


##### target rules #####


def get_table_params_as_list() -> list[str]:
    return (
        samples.apply(lambda row: f"{row['pp_type']}_pp_{row['model_type']}", axis=1)
        .to_numpy()
        .tolist()
    )


envvars:
    "MAX_CORES",


rule all:
    input:
        expand(
            "workflow/results/adult_{table_format}_format_train_val_split_{preprocessor_with_model}.model.pickle",
            table_format=config["table_params"]["table_format"],
            preprocessor_with_model=get_table_params_as_list(),
        ),
