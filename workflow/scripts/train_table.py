#!/usr/bin/env python


import pathlib
import pickle
import traceback
from contextlib import redirect_stderr, redirect_stdout

import hydra
from hydra.utils import instantiate
from mlops_in_production.dataset.read import read_table_dataset
from omegaconf import DictConfig


@hydra.main(
    version_base=None,
    config_path=str(
        pathlib.Path(__file__).parent.parent.parent / "config/hydra/modeling"
    ),
    config_name=snakemake.params.model_name,
)
def main(config: DictConfig):
    dataset_path = pathlib.Path(str(snakemake.input))
    dataset = read_table_dataset(dataset_path, snakemake.params.table_format)

    print(f"Model type: {config.name}")
    output_model = instantiate(config.model).fit(dataset, parallelism=snakemake.threads)
    with open(str(snakemake.output.model), "wb") as model_f:
        pickle.dump(output_model, model_f)


if __name__ == "__main__":
    with (
        open(snakemake.log.out, "w") as new_stdout,
        open(snakemake.log.err, "w") as new_stderr,
        redirect_stdout(new_stdout),
        redirect_stderr(new_stderr),
    ):
        try:
            main()
        except BaseException as e:
            print(traceback.format_exc())
            raise
