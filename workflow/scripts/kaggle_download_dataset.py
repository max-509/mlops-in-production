#!/usr/bin/env python

import pathlib
import traceback
from contextlib import redirect_stderr, redirect_stdout

import kaggle

workflow_path = pathlib.Path(__file__).parent.parent


def main():
    user_name = snakemake.params.user_name
    dataset_name = snakemake.params.dataset_name
    output_path = pathlib.Path(str(snakemake.output)).parent

    kaggle.api.authenticate()

    kaggle.api.dataset_download_files(
        f"{user_name}/{dataset_name}",
        path=str(output_path),
        quiet=False,
        force=True,
    )


if __name__ == "__main__":
    with (
        open(snakemake.log.out, "w") as new_stdout,
        open(snakemake.log.err, "w") as new_stderr,
        redirect_stdout(new_stdout),
        redirect_stderr(new_stderr),
    ):
        try:
            main()
        except BaseException as e:
            print(traceback.format_exc())
            raise
