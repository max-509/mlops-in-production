#!/usr/bin/env python


import pathlib
import traceback
from contextlib import redirect_stderr, redirect_stdout

from mlops_in_production.dataset.read import read_table_dataset
from mlops_in_production.dataset.write import save_table_dataset
from mlops_in_production.split.split import train_val_split


def main():
    dataset_path = pathlib.Path(str(snakemake.input))
    full_dataset = read_table_dataset(
        dataset_path=dataset_path, table_format=snakemake.params.table_format
    )
    splitted_df = train_val_split(
        full_dataset,
        stratified=snakemake.params.stratified,
        val_size=snakemake.params.val_size,
        seed=snakemake.params.seed,
    )
    save_table_dataset(
        splitted_df,
        table_format=snakemake.params.table_format,
        output_path=pathlib.Path(str(snakemake.output)),
    )


if __name__ == "__main__":
    with (
        open(snakemake.log.out, "w") as new_stdout,
        open(snakemake.log.err, "w") as new_stderr,
        redirect_stdout(new_stdout),
        redirect_stderr(new_stderr),
    ):
        try:
            main()
        except BaseException as e:
            print(traceback.format_exc())
            raise
