#!/usr/bin/env python

import pathlib
import pickle
import traceback
from contextlib import redirect_stderr, redirect_stdout

import hydra
from hydra.utils import instantiate
from mlops_in_production.dataset.metadata import TableDataset
from mlops_in_production.dataset.read import read_table_dataset
from mlops_in_production.dataset.write import save_table_dataset
from omegaconf import DictConfig


@hydra.main(
    version_base=None,
    config_path=str(
        pathlib.Path(__file__).parent.parent.parent / "config/hydra/preprocessing"
    ),
    config_name=snakemake.params.preprocessor_name,
)
def main(config: DictConfig):
    dataset_path = pathlib.Path(str(snakemake.input))
    dataset_for_preprocess = read_table_dataset(
        dataset_path, table_format=snakemake.params.table_format
    )

    print(f"Preprocessor type: {config.name}")
    preprocessor = instantiate(config.preprocessor).fit(dataset_for_preprocess)
    pp_train_df = preprocessor.transform(dataset_for_preprocess.train_df)
    pp_val_df = None
    if dataset_for_preprocess.val_df is not None:
        pp_val_df = preprocessor.transform(dataset_for_preprocess.val_df)

    pp_metadata = preprocessor.output_metadata_

    pp_dataset = TableDataset(
        train_df=pp_train_df, metadata=pp_metadata, val_df=pp_val_df
    )
    save_table_dataset(
        pp_dataset,
        table_format=snakemake.params.table_format,
        output_path=pathlib.Path(snakemake.output.dataset),
    )

    with open(str(snakemake.output.preprocessor), "wb") as preprocessor_f:
        pickle.dump(preprocessor, preprocessor_f)


if __name__ == "__main__":
    with (
        open(snakemake.log.out, "w") as new_stdout,
        open(snakemake.log.err, "w") as new_stderr,
        redirect_stdout(new_stdout),
        redirect_stderr(new_stderr),
    ):
        try:
            main()
        except BaseException as e:
            print(traceback.format_exc())
            raise
