#!/usr/bin/env python

import pathlib
import traceback
import zipfile
from contextlib import redirect_stderr, redirect_stdout
from tempfile import TemporaryDirectory

import pandas as pd
from mlops_in_production.dataset.metadata import TableDataset, TableMetadata
from mlops_in_production.dataset.write import save_table_dataset

workflow_path = pathlib.Path(__file__).parent.parent


def main():
    adult_dataset_path = pathlib.Path(str(snakemake.output))

    # metadata
    task_type = "CLASSIFICATION"
    target_col = "income"
    cat_features = [
        "workclass",
        "education",
        "marital-status",
        "occupation",
        "relationship",
        "race",
        "native-country",
    ]
    num_features = [
        "age",
        "educational-num",
        "fnlwgt",
        "capital-gain",
        "capital-loss",
        "hours-per-week",
    ]
    bin_features = ["gender"]

    metadata = TableMetadata(
        num_features=num_features,
        cat_features=cat_features,
        bin_features=bin_features,
        target_col=target_col,
        task_type=task_type,
    )

    # data
    adult_zip_path = pathlib.Path(str(snakemake.input))

    with (
        zipfile.ZipFile(adult_zip_path, "r") as zip_ref,
        TemporaryDirectory() as tmp_dir,
    ):
        tmp_path = pathlib.Path(tmp_dir)
        zip_ref.extractall(tmp_dir)

        df = pd.read_csv(tmp_path / "adult.csv")

    dataset = TableDataset(train_df=df, metadata=metadata)

    save_table_dataset(dataset, snakemake.params.table_format, adult_dataset_path)


if __name__ == "__main__":
    with (
        open(snakemake.log.out, "w") as new_stdout,
        open(snakemake.log.err, "w") as new_stderr,
        redirect_stdout(new_stdout),
        redirect_stderr(new_stderr),
    ):
        try:
            main()
        except BaseException as e:
            print(traceback.format_exc())
            raise
