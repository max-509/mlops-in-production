from mlops_in_production.source import summator


def test_summator():
    assert summator((1, 2, 3)) == 6
